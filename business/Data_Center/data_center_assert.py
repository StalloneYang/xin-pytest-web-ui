# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from common.logger import Logger
from base.base import BasePage
from selenium.webdriver.support import expected_conditions as EC

class DataCenterAssert:

    def __init__(self, driver):
        self.driver = driver

    # 断言

    def data_storage_assert(self):
        try:
            WebDriverWait(self.driver,15).\
                until(EC.visibility_of_element_located((By.XPATH,'//*[@id="mainScreen"]/div/div/div/div[1]/div[1]/div')))
            Logger.info("进入数据存储页面成功，用例执行成功")
            return True
        except:
            Logger.info("断言失败，用例执行失败")
            return False

    # def data_center_assert(self, text):
    #     return BasePage(self.driver).assert_text(
    #         (By.XPATH, '//div[@class="notify-content"]'), text, doc="提示" + text)

