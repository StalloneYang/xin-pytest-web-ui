# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By


class LoginLocator:
    """登录模块"""
    #密码登录
    paswd_login = (By.XPATH, '/html/body/div[1]/div[1]/div[2]/div/div[1]/div[2]/div[1]/div/div[2]')
    #账户
    username = (By.XPATH, '/html/body/div[1]/div[1]/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[1]/input')
    #密码
    password = (By.XPATH, '/html/body/div[1]/div[1]/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/input')
    #登录按钮
    login_button = (By.XPATH, '/html/body/div[1]/div[1]/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[6]/button')

    # @staticmethod
    # def order_kind(title):
    #     kind = (By.XPATH, '//*[@title="%s"]' % title)
    #     return kind
    #
    # @staticmethod
    # def order_operation(text):
    #     operation = (By.XPATH, '//li[text()="%s"]' % text)
    #     return operation
    #
    # @staticmethod
    # def operator_btn(text):
    #     operator = (By.XPATH, '//*[@id="orderOperatorBtns"]//*[text()="%s"]' % text)
    #     return operator
